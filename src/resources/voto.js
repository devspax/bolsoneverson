import Api from './Api'

export default {
  votar(body) {
    return Api().post(`/vote`, body)
  },
  listar() {
    return Api().get(`/vote`)
  },
  votos() {
    return Api().get(`/votos`)
  }
}