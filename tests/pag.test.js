import { Selector } from 'testcafe'
import axios from 'axios'

const url = `http://localhost:8080`

fixture(`Index page`)
  .page(url)

test('Contem uma pergunta', async testController => {
  // Select the paragraph element under the body.
  // Must use promises (async / await  here) for communication with the browser.
  const paragraphSelector = await new Selector('#step-0 p')
  await testController.expect(paragraphSelector.innerText).eql('Insira seu nome')
})

test('Insere e envia', async t => {

  const insere = () => new Promise(async (resolve, reject) => {
    const textBox = await new Selector('#step-0 input')

    let data = (await axios({
      method: 'get',
      url: 'https://randomuser.me/api/',
    })).data

    data = data.results[0]

    const name = `${data.name.first} ${data.name.last}`
    await t
      .typeText(textBox, name)

    const nextBtn = await new Selector('#next-btn button')
    await t.click(nextBtn)
    
    let simOunao = Math.round(Math.random())

    const btn = await new Selector(`#btn-${ simOunao ? 'facista' : 'naofacista' }`)
    await t.click(btn).navigateTo(url)
    resolve()
  })
  
  let pall = []
  let i
  for (i=0; i < 100000; i++) {
    insere()
  }
})