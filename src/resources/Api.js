import axios from 'axios'
const base = `${process.env.VUE_APP_HTTP_PATH}/back`

export default () => {
  return axios.create({
    baseURL: base,
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}